FROM centos

COPY amq-broker-7.8.2-bin.zip .

RUN yum install unzip -y
RUN yum install java-11-openjdk -y

RUN unzip amq-broker-7.8.2-bin.zip -d .

RUN ./amq-broker-7.8.2/bin/artemis create --allow-anonymous --password test --user test ./mybroker

EXPOSE 61616 5445 5672 1883 61613 8080 8161

CMD /mybroker/bin/artemis run






